﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Identity_Jwt_Demo
{
    public class JwtSetings
    {
        /// <summary>
        /// 颁发者
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// 接受者
        /// </summary>
        public string Audience { get; set; }


        /// <summary>
        /// 过期时期（分钟）
        /// </summary>
        public int ExpireMinuters { get; set; }

        /// <summary>
        /// 加密算法
        /// RSA,DES
        /// </summary>
        public string SecretType { get; set; }

        /// <summary>
        /// secret是保存在服务器端的，jwt的签发生成也是在服务器端的，secret就是用来进行jwt的签发和jwt的验证，
        /// 所以，它就是你服务端的私钥，在任何场景都不应该流露出去。一旦客户端得知这个secret, 那就意味着客户端是可以自我签发jwt了
        /// 通过jwt header中声明的加密方式进行加盐secret组合加密，然后就构成了jwt的第三部分
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// 发放凭证的私钥
        /// </summary>
        public string PrivateKey { get; set; }

        /// <summary>
        /// 验证凭证签名的公钥
        /// </summary>
        public string PublicKey { get; set; }


        private static RSA key = RSA.Create(2048);

        private RSA _PrivateRSA;

        /// <summary>
        /// 发放凭证的私钥
        /// </summary>
        public RSA PrivateRSA
        {
            get
            {
                if (_PrivateRSA == null)
                {
                    _PrivateRSA = key;
                    //_PrivateRSA = RSA.Create();
                    //_PrivateRSA.FromXmlString(PrivateKey);
                }
                return _PrivateRSA;
            }
        }
        
        private RSA _PublicRSA;
        /// <summary>
        /// 验证凭证签名的公钥
        /// </summary>
        public RSA PublicRSA
        {
            get
            {
                if (_PublicRSA == null)
                {
                    _PublicRSA = RSA.Create(key.ExportParameters(false));
                    //_PublicRSA = RSA.Create();
                    //_PublicRSA.FromXmlString(PublicKey);
                }
                return _PublicRSA;
            }
        }
    }

}
