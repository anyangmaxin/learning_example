using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration;

namespace EntityFrameWorkDAL
{


    public partial class DBContext : DbContext
    {
        public DBContext()
            :this("name=DB")
        {
        }

        public DBContext(string NameOrConnectionString)
            : base(NameOrConnectionString)
        {
             Database.SetInitializer<DBContext>(new MigrateDatabaseToLatestVersion<DBContext,DataBaseConfiguration>());//模型变更时自动两步数据库
           // Database.SetInitializer<DBContext>(new DropCreateDatabaseIfModelChanges<DBContext>()); //模型变更时删除重建数据库
           // Database.SetInitializer<DBContext>(new DropCreateDatabaseAlways<DBContext>()); //永远删除重建数据库
           // Database.SetInitializer<DBContext>(new CreateDatabaseIfNotExists<DBContext>()); //数据库不存在时新建
           // Database.SetInitializer<DBContext>(new NullDatabaseInitializer<DbContext>());   //不做任何事情，包括检查也不做
        }

        internal sealed class DataBaseConfiguration : DbMigrationsConfiguration<DBContext>
        {
            public DataBaseConfiguration()
            {
                AutomaticMigrationsEnabled = true;
                AutomaticMigrationDataLossAllowed = true;
            }

            protected override void Seed(DBContext context)
            {
                base.Seed(context);
            }
        }

        public virtual DbSet<Model.ManagerModel> Managers { get; set; }
        public virtual DbSet<Model.GroupModel> Groups { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var mapTypes = Common.ClassTypeUtils.FindImplementedTypes(typeof(EntityTypeConfiguration<>));
            foreach (var maptype in mapTypes)
            {
                dynamic map = Common.ClassTypeUtils.Instance(maptype);
                modelBuilder.Configurations.Add(map);
            }
            base.OnModelCreating(modelBuilder);
        }
    }
}
