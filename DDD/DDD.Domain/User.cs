﻿using System;

namespace DDD.Domain
{
    public class User
    {
        public string UserName { get; protected set; }

        public string Password { get; protected set; }

        public bool IsDisabled { get; protected set; }

        protected User()
        {

        }

        public void ChangePassword(string oldPassword,string newPassword)
        {
            if (this.Password != oldPassword) throw new Exception("旧密码错误");
            this.Password = newPassword;
            
        }
        
        public void Disable()
        {
            this.IsDisabled = true;
        }

        public void Enable()
        {
            this.IsDisabled = false;
        }

    }
}
