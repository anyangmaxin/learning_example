﻿using DotNetCore.CAP;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Savorboard.CAP.InMemoryMessageQueue;
using System;

namespace CAPConsoleDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();  //构建一个常规Host
        }
        static IHostBuilder CreateHostBuilder(string[] args)
        {
            var build = new HostBuilder();
            build.ConfigureServices((hostContext, services) =>
            {
                services.AddCap(option =>      //添加CAP框架
                {
                    option.UseInMemoryStorage();   //使用内存存储
                    option.UseInMemoryMessageQueue();  //使用内存队列
                    option.UseDashboard();  //添加监控仪表盘
                });

                services.AddHostedService<Publish>();  //添加推送程序

                services.AddSingleton<ICapSubscribe, Events>();  //添加监听服务

            });
            return build;
        }
    }
}
